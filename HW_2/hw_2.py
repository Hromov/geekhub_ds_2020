import numpy as np

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
iris = np.genfromtxt(url, delimiter=',', dtype='object')
names = ('sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species')

# p.1. - removing 'species' column
iris_no_species = np.delete(iris, 4, axis=1)
print(iris_no_species)

# p.2. - first 4 columns into 2d.
iris_2d = np.array(iris_no_species, dtype='float')

print()
print(iris_2d)

# p.3. Calculate mean, median, standard deviation by 1 column
column_1 = iris_2d[:, 0]
mean = np.mean(column_1)
median = np.median(column_1)
std = np.std(column_1)

print()
print("The 'mean' value is:", mean)
print("The 'median' value is:", median)
print("The 'standard deviation' value is:", std)

# p.4. Insert 20 random 'nan' values
iris_2d[np.random.randint(150, size=20), np.random.randint(4, size=20)] = np.nan
print()
print(iris_2d)

# p.5. """Locate nan positions in column 1. Each time nan value will be in different place.
# It's good to know how much nan values are in column 1 each time"""
nan_sum = np.isnan(iris_2d[:, 0]).sum()
nan_position = np.where(np.isnan(iris_2d[:, 0]))

print()
print(nan_sum)
print()
print(nan_position)

# p.6. Filter an array by condition
condition = (iris_2d[:, 2] > 1.5) & (iris_2d[:, 0] < 5.0)
print()
print(iris_2d[condition])

# p.7. Replace nun with 0
iris_2d[np.isnan(iris_2d)] = 0

# p.8. Count all unique values
(unique, counts) = np.unique(iris, return_counts=True)
frequencies = np.asarray((unique, counts)).T

print()
print(frequencies)

# p.9. Divide array horizontally into 2 equal parts
split_h = np.vsplit(iris_2d, 2)

# p.10. Sort 1st column of 1st array ascending and 2nd column of 2nd array descending
array_1 = split_h[0]
array_2 = split_h[1]

sorted_1stcol = array_1[:, 0].argsort()
sorted1 = array_1[sorted_1stcol]
print()
print(sorted1)

sorted_2ndcol = array_2[:, 1].argsort()
sorted2 = array_2[sorted_2ndcol[::-1]]
print()
print(sorted2)

# p.11. 'Glue' arrays back into one
glued_array = np.concatenate([sorted1, sorted2], axis=1)
print()
print(glued_array)

# p.12. Find the most frequent value
v, c = np.unique(glued_array[:, 2], return_counts=True)
print()
print('The most frequent value is:', v[np.argmax(c)])

# p.13. Create a function, that will multiple all values in a column that are less than
mean_3d_col = np.mean(glued_array[:, 2])

def double(x):
    if x < mean_3d_col:
        return x * 2
    elif x > mean_3d_col:
        return x / 4

double_v = np.vectorize(double, otypes=[float])

print()
print("The mean value of column 3 is:", mean_3d_col)
print()
print('Function applied: ', np.apply_along_axis(double_v, 0, arr=glued_array[:, 2]))
#Another option is print('Function applied: ', double_v(glued_array[:, 2]))